package com.example.calculatrice;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView res;
    private String operation;
    private double savedValue=0;
    private boolean dicimal=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        res= (TextView) findViewById(R.id.result);
    }

    //Opération

    public void add(View v){
        operation="+";
        savedValue=Double.parseDouble(res.getText().toString());
        res.setText(""+0);
        dicimal=false;
    }

    public void sub(View v){
        operation="-";
        savedValue=Double.parseDouble(res.getText().toString());
        res.setText(""+0);
        dicimal=false;
    }

    public void prod(View v){
        operation="*";
        savedValue=Double.parseDouble(res.getText().toString());
        res.setText(""+0);
        dicimal=false;
    }

    public void divide(View v){
        operation="/";
        savedValue=Double.parseDouble(res.getText().toString());
        res.setText(""+0);
        dicimal=false;
    }

    public void equal(View v){
        double val=Double.parseDouble(res.getText().toString());
        double result=0;
            switch (operation){
                case "/":
                    if(val==0){break;}
                    result=savedValue/val;
                    break;
                case "*":
                    result=savedValue*val;
                    break;
                case "-":
                    result=savedValue-val;
                    break;
                case "+":
                    result=savedValue+val;
                    break;
                default:
                    result=0;
            }
        res.setText(""+result);
        operation="";
        dicimal=false;
    }

    public void dicimal(View v){
        if(dicimal==false) {
            res.setText(res.getText().toString() + ".");
            dicimal = true;
        }
    }
    //RESET Button
    public void reset(View v){
        res.setText(""+0);
        savedValue=0;
        operation="";
        dicimal=false;
    }

    //Clavier numérique

    public void number0(View v){
        if((res.getText().toString().equals("0"))){
            res.setText("0");
        }else {
            res.setText(res.getText().toString() + "0");
        }
    }
    public void number1(View v){
        if((res.getText().toString().equals("0"))) {
            res.setText("1");
        }
        else {
            res.setText(res.getText().toString() + "1");
        }
    }
    public void number2(View v){
        if((res.getText().toString().equals("0"))) {
            res.setText("2");
        }
        else {
            res.setText(res.getText().toString() + "2");
        }    }
    public void number3(View v){
        if((res.getText().toString().equals("0"))) {
            res.setText("3");
        }
        else {
            res.setText(res.getText().toString() + "3");
        }
    }
    public void number4(View v){
        if((res.getText().toString().equals("0"))) {
            res.setText("4");
        }
        else {
            res.setText(res.getText().toString() + "4");
        }
    }
    public void number5(View v){
        if((res.getText().toString().equals("0"))) {
            res.setText("5");
        }
        else {
            res.setText(res.getText().toString() + "5");
        }
    }
    public void number6(View v){
        if((res.getText().toString().equals("0"))) {
            res.setText("6");
        }
        else {
            res.setText(res.getText().toString() + "6");
        }
    }
    public void number7(View v){
        if((res.getText().toString().equals("0"))) {
            res.setText("7");
        }
        else {
            res.setText(res.getText().toString() + "7");
        }
    }
    public void number8(View v){
        if((res.getText().toString().equals("0"))) {
            res.setText("8");
        }
        else {
            res.setText(res.getText().toString() + "8");
        }
    }
    public void number9(View v){
        if((res.getText().toString().equals("0"))) {
            res.setText("9");
        }
        else {
            res.setText(res.getText().toString() + "9");
        }
    }
}
